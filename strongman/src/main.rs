use std::time::Duration;

enum WeightMetric {
    Lb(isize),
    Kg(isize),
}

enum DistanceMeasure {
    Miles(isize),
    Yards(isize),
    Feet(isize),
    Inches(isize),
    Kilometers(isize),
    Meters(isize),
}

enum WorkType {
    Reps(isize),
    Distance(DistanceMeasure),
    Height(DistanceMeasure),
    Time(std::time::Duration),
}

struct WorkSet {
    weight: WeightMetric,
    reps: WorkType,
}

struct ExerciseWorkSets {
    id: String,
    work: Vec<WorkSet>,
    rest: std::time::Duration,
}

fn main() {
    println!("Hello, world!");
}
