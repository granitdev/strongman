use super::conversion_trait::Conversion;

pub enum Weight {
    lb,
    oz,
    kg,
}

impl Conversion for Weight {
    fn conv_factor(&self) -> f32 {
        match *self {
            Weight::lb => {1.0}
            Weight::oz => {0.0625}
            Weight::kg => {2.2}
        }
    }

    // pub fn value(&self) -> f32 {
    //     match *self {
    //         MassUnit::lb(value) => {value}
    //         MassUnit::kg(value) => {value}
    //     }
    // }
}