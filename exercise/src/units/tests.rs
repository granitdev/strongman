use super::weight::*;
use super::linear::*;
use super::unit::*;

#[cfg(test)]

#[test]
fn weight_conversion() {
    let pound = Unit::new(2.2, Weight::lb);
    let ounce = Unit::new(16.0, Weight::oz);
    let kilo = Unit::new(1.0, Weight::kg);

    assert_eq!(pound.to(Weight::kg).value, 1.0);
    assert_eq!(ounce.to(Weight::lb).value, 1.0);
    assert_eq!(kilo.to(Weight::lb).value, 2.2);
}

fn linear_conversion() {
    let yard = Unit::new(3.0, Linear::yd);
    let foot = Unit::new(1.0, Linear::ft);
    let inch = Unit::new(0.08333, Linear::inch);

    assert_eq!(yard.to(Linear::ft).value, 3.0);
}