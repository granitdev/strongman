use super::conversion_trait::Conversion;

pub enum Linear {
    yd,
    ft,
    inch,
    m,
    cm,
    mm,
}

impl Conversion for Linear {
    fn conv_factor(&self) -> f32 {
        match *self {
            Linear::yd => {3.0}
            Linear::ft => {1.0}
            Linear::inch => {0.08333}
            Linear::m => {3.28084}
            Linear::cm => {0.032808}
            Linear::mm => {0.00328}
        }
    }
}