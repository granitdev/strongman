pub trait Conversion {
    fn conv_factor(&self) -> f32;
}