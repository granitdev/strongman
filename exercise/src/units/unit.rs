use super::conversion_trait::Conversion;

pub struct Unit<T: Conversion> {
    pub value: f32,
    pub unit: T,
}

impl<T: Conversion> Unit<T> {
    pub fn new(value: f32, unit: T) -> Unit<T> {
        Unit {value, unit}
    }
    pub fn to(&self, u: T) -> Unit<T> {
        let self_cv: f32 = self.unit.conv_factor();
        let to_cv: f32 = u.conv_factor();
        let tu = Unit {
            value: self.value * self_cv / to_cv,
            unit: u,
        };
        tu
    }
}