mod units;
use crate::units::weight::*;
use crate::units::unit::*;
use std::time::Duration;

enum DistanceMeasure {
    Miles(isize),
    Yards(isize),
    Feet(isize),
    Inches(isize),
    Kilometers(isize),
    Meters(isize),
}

enum WorkType {
    Reps(isize),
    Distance(DistanceMeasure),
    Height(DistanceMeasure),
    Time(std::time::Duration),
}

struct WorkSet {
    weight: Unit<Weight>,
    reps: WorkType,
}

struct ExerciseWorkSet {
    id: String,
    work: Vec<WorkSet>,
    rest: std::time::Duration,
}
